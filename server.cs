﻿//anggota : 1. 4210181020_Ilham Pratama
//          2. 4210181023_Ilham Agung Riyadi  

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace IncrementString
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpClient client = new UdpClient(8080);   
            IPAddress clientIP = IPAddress.Parse("127.0.0.1");
            IPEndPoint remoteEndPoint = new IPEndPoint(clientIP, 12345);
            IPEndPoint recv = new IPEndPoint(clientIP, 12345);

            while (true)
            {
                
                byte[] data = client.Receive(ref recv);
                string a = Encoding.ASCII.GetString(data);
                Console.WriteLine("Pesan Client : " + a);
                char[] b = a.ToCharArray();

                Console.Write("Setelah Diubah : ");
                for (int i = 0; i < b.Length; i++)
                {                   
                    char c = b[i];

                    if (c != 32 || c < 122)
                    {
                        c += (char)4;
                    }

                    if ( c > 122 )
                    {
                        c -= (char)26;
                        Console.Write(c);
                    }

                    string d = Convert.ToString(c);
                    byte[] datasend = Encoding.ASCII.GetBytes(d);
                    client.Send(datasend, datasend.Length, remoteEndPoint);
                }
                Console.WriteLine();
                Console.WriteLine("Dikirim");
                Console.WriteLine();
            }     
        }
    }
}
