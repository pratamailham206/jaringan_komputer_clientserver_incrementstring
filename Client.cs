﻿//anggota : 1. 4210181020_Ilham Pratama
//          2. 4210181023_Ilham Agung Riyadi            

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpClient client = new UdpClient(12345);
            IPAddress serverIP = IPAddress.Parse("127.0.0.1");
            IPEndPoint remoteEndPoint = new IPEndPoint(serverIP, 8080);
            IPEndPoint recv = new IPEndPoint(serverIP, 8080);


            while (true)
            {
                Console.Write("Input Alphabet : ");
                string alpha = Console.ReadLine();
                byte[] datasend = Encoding.ASCII.GetBytes(alpha);
                client.Send(datasend, datasend.Length, remoteEndPoint);

                char[] a = alpha.ToCharArray();

                Console.Write("Setelah Diubah : ");
                for (int i = 0; i < a.Length; i++)
                {
                    byte[] data = client.Receive(ref recv);
                    string remote = recv.Address.ToString();
                    string incrementAlphabet = Encoding.ASCII.GetString(data);
                    Console.Write(incrementAlphabet);
                }
                Console.WriteLine();
            }
        }
    }
}
